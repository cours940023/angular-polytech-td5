import { Component, Input } from '@angular/core';
import { Vol } from '../../models/vol.model';
import { MatIconModule } from '@angular/material/icon';
import { NomCompagnieComponent } from '../nom-compagnie/nom-compagnie.component';

@Component({
  selector: 'app-vol',
  standalone: true,
  imports: [MatIconModule, NomCompagnieComponent],
  templateUrl: './vol.component.html',
  styleUrls: ['./vol.component.scss']
})
export class VolComponent {
  @Input({required: true}) vol!: Vol;
  @Input() atterrissage?: boolean;
}
