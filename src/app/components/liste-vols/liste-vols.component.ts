import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Vol } from '../../models/vol.model';
import { VolComponent } from '../vol/vol.component';

@Component({
  selector: 'app-liste-vols',
  standalone: true,
  imports: [VolComponent],
  templateUrl: './liste-vols.component.html',
  styleUrls: ['./liste-vols.component.scss']
})
export class ListeVolsComponent {
  @Input({required: true}) vols: Vol[] = [];
  @Input() atterrissage?: boolean;

  @Input() volSelectionne?: Vol;
  @Output() volSelectionneChange: EventEmitter<Vol> = new EventEmitter<Vol>();

  selectionnerVol(vol: Vol): void {
    this.volSelectionneChange.emit(vol);
  }
}
