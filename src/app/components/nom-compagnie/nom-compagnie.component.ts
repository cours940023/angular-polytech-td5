import { Component, Input, SimpleChanges } from '@angular/core';

@Component({
  selector: 'app-nom-compagnie',
  standalone: true,
  imports: [],
  templateUrl: './nom-compagnie.component.html',
  styleUrl: './nom-compagnie.component.scss'
})
export class NomCompagnieComponent {
  @Input() compagnie?: string;

  src: string = '';

  constructor() { }

  ngOnChanges(changes: SimpleChanges): void {
    this.updateImageSrc();
  }

  updateImageSrc(): void {
    switch(this.compagnie) {
      case 'Air France':
        this.src = '/assets/Air France.png';
        break;

      case 'Transavia France':
        this.src = '/assets/Transavia France.png';
        break;

      case 'Air France Hop':
        this.src = '/assets/Air France Hop.png';
        break;

      default:
        this.src = '';
    }
  }
}
