import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NomCompagnieComponent } from './nom-compagnie.component';

describe('NomCompagnieComponent', () => {
  let component: NomCompagnieComponent;
  let fixture: ComponentFixture<NomCompagnieComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [NomCompagnieComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(NomCompagnieComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
