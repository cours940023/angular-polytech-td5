import { Component, Input } from '@angular/core';
import { Passager } from '../../models/passager.model';
import { MatIconModule } from '@angular/material/icon';
import { CouleurClasseDirective } from '../../directives/couleur-classe.directive';
import { ExcedentBagageDirective } from '../../directives/excedent-bagage.directive';
import { MatTooltipModule } from '@angular/material/tooltip';

@Component({
  selector: 'app-passager',
  standalone: true,
  imports: [MatIconModule, CouleurClasseDirective, ExcedentBagageDirective, MatTooltipModule],
  templateUrl: './passager.component.html',
  styleUrls: ['./passager.component.scss']
})
export class PassagerComponent {
  @Input({required: true}) passager!: Passager;
  @Input() affichagePhotos: boolean = false;
}
