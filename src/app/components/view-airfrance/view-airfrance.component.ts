import { Component } from '@angular/core';
import { FiltresComponent } from '../filtres/filtres.component';
import { ListeVolsComponent } from '../liste-vols/liste-vols.component';
import { ListePassagersComponent } from '../liste-passagers/liste-passagers.component';
import { IFiltres } from '../../models/filtres.model';
import { Vol } from '../../models/vol.model';
import { VolService } from '../../services/vol.service';
import { Passager } from '../../models/passager.model';
import { PassagerService } from '../../services/passager.service';
import { ActivatedRoute } from '@angular/router';
import { Observable, map } from 'rxjs';
import { MODE_ATTERISSAGES, TITRE_DEFAUT } from '../../constants/valeurs.constants';
import { MatSlideToggleChange, MatSlideToggleModule } from '@angular/material/slide-toggle';

@Component({
  selector: 'app-view-airfrance',
  standalone: true,
  imports: [FiltresComponent, ListeVolsComponent, ListePassagersComponent, MatSlideToggleModule],
  templateUrl: './view-airfrance.component.html',
  styleUrls: ['./view-airfrance.component.scss']
})
export class ViewAirFranceComponent {
  vols: Vol[] = [];
  volSelectionne?: Vol;
  passagers: Passager[] = [];
  modeObservable!: Observable<string>;
  mode: string = '';
  titre: string = TITRE_DEFAUT;
  affichagePhotos: boolean = false;

  constructor(
    private volService: VolService, 
    private passagerService: PassagerService, 
    private activatedRoute: ActivatedRoute
  ) {
    this.modeObservable = this.activatedRoute.params.pipe(map((p) => p['mode']));
    this.modeObservable.subscribe((mode) => this.miseAJourMode(mode));
  }

  /**
   * Filtre les vols en fonction des critères de recherche
   * @param filtres les critères de recherche
   */
  filtrer(filtres: IFiltres) {
    this.volSelectionne = undefined;
    this.passagers = [];
    if(this.mode === MODE_ATTERISSAGES) {
      this.volService.getVolsArriveeAeroport(filtres.aeroport, filtres.debut, filtres.fin).subscribe((vols) => {
        this.vols = vols;
      });
    }
    else {
      this.volService.getVolsDepartAeroport(filtres.aeroport, filtres.debut, filtres.fin).subscribe((vols) => {
        this.vols = vols;
      });
    }
  }

  /**
   * Sélectionne un vol et récupère les passagers associés
   */
  selectionnerVol(vol: Vol): void {
    this.volSelectionne = vol;
    this.passagerService.getPassagersVol(vol.icao).subscribe((passagers) => {
      this.passagers = passagers;
    });
  }

  /**
   * Met à jour le mode des vols (départ ou arrivée)
   * @param mode le mode des vols
   */
  miseAJourMode(mode: string): void {
    this.mode = mode;
    this.titre = mode === MODE_ATTERISSAGES ? 'ATTÉRRISSAGES' : TITRE_DEFAUT;
    this.vols = [];
    this.volSelectionne = undefined;
    this.passagers = [];
  }

  /**
   * Modifie l'affichage des photos des passagers
   */
  modifierAffichagePhotos(valeur: MatSlideToggleChange): void {
    this.affichagePhotos = valeur.checked;
  }
}
