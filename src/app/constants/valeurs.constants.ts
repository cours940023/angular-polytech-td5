export const TITRE_DEFAUT = 'DÉCOLLAGES';
export const MODE_ATTERISSAGES = 'atterrissages';
export const MODE_DECOLLAGES = 'decollages';

export const COULEUR_STANDARD = '#0000aa';
export const COULEUR_BUSINESS = '#aa0000';
export const COULEUR_PREMIUM = '#00aa00';