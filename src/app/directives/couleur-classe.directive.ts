import { Directive, ElementRef, Input } from '@angular/core';
import { ClasseVol } from '../models/passager.model';
import { COULEUR_BUSINESS, COULEUR_PREMIUM, COULEUR_STANDARD } from '../constants/valeurs.constants';

@Directive({
  selector: '[couleurClasse]',
  standalone: true
})
export class CouleurClasseDirective {
  @Input() couleurClasse?: string;

  constructor(private el: ElementRef<HTMLElement>) { 
    this.miseAJourCouleur();
  }

  ngOnChanges(): void {
    this.miseAJourCouleur();
  }

  /**
   * Met à jour la couleur du texte en fonction de la classe du passager.
   */
  miseAJourCouleur(): void {
    let couleur = 'black';
    switch (this.couleurClasse) {
      case 'STANDARD':
        couleur = COULEUR_STANDARD;
        break;
      case 'BUSINESS':
        couleur = COULEUR_BUSINESS;
        break;
      case 'PREMIUM':
        couleur = COULEUR_PREMIUM;
        break;
    }
    this.el.nativeElement.style.color = couleur;
  }
}
