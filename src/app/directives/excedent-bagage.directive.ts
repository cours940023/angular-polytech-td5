import { Directive, ElementRef, Input } from '@angular/core';

@Directive({
  selector: '[excedentBagage]',
  standalone: true
})
export class ExcedentBagageDirective {
  @Input() nombreBagages?: number;
  @Input() classe?: string;

  constructor(private el: ElementRef<HTMLElement>) { 
    this.miseAJourCouleur();
  }

  ngOnChanges(): void {
    this.miseAJourCouleur();
  }

  /**
   * Met à jour la couleur de fond en fonction du nombre de bagages et de la classe du passager.
   */
  miseAJourCouleur(): void {
    if(!this.nombreBagages || !this.classe) {
      this.el.nativeElement.style.background = 'unset';
      return;
    }

    const bagagesMax = this.calculerBagagesMax(this.classe);
    if(this.nombreBagages > bagagesMax) {
      this.el.nativeElement.style.background = 'red';
    }
  }

  /**
   * Calcule le nombre de bagages maximum en fonction de la classe du passager.
   * @param classe la classe du passager
   * @returns le nombre de bagages maximum, 1 par défaut
   */
  private calculerBagagesMax(classe?: string): number {
    switch (classe) {
      case 'STANDARD':
        return 1;
      case 'BUSINESS':
        return 2;
      case 'PREMIUM':
        return 3;
    }
    return 1;
  }
}
