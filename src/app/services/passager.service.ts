import { Injectable } from '@angular/core';
import { IPassagerDto, Passager } from '../models/passager.model';
import { Observable, map } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class PassagerService {

  constructor(private httpClient: HttpClient) { }

  /**
   * Récupère des passagers aléatoires à partir de l'api randomuser.me
   * @param icaoVol le code ICAO du vol
   * @returns une liste de 20 passagers avec leur nom, leur photo et leur email
   */
  getPassagersVol(icaoVol: string): Observable<Passager[]> {
    return this.httpClient.get<any>(`https://randomuser.me/api?results=20&inc=name,picture,email&seed=${icaoVol}`)
      .pipe(map((response: { results: IPassagerDto[] }) => response.results.map(passager => new Passager(passager))));
  }
}
