import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map, Observable } from 'rxjs';
import { COMPAGNIES } from './../constants/compagnie.constant';
import { IVolDto, Vol } from './../models/vol.model';
import { IAeroport } from '../models/aeroport.model';

@Injectable({
  providedIn: 'root'
})
export class VolService {

  constructor(private http: HttpClient) { }

  /**
   * Récupération de la liste des vols en départ d'un aéroport donné en paramètre et selon un intervalle de temps donné.
   * Open Sky REST API
   * https://openskynetwork.github.io/opensky-api/rest.html#departures-by-airport
   */
  getVolsDepart(code: string, debut: number, fin: number): Observable<Vol[]> {
    return this.http.get<any>(`https://opensky-network.org/api/flights/departure?airport=${code}&begin=${debut.toFixed()}&end=${fin.toFixed()}`).pipe(
      map((response) => response
        .map((dto: IVolDto) => new Vol(dto))
    ));
  }

    /**
   * Récupération de la liste des vols en départ d'un aéroport donné en paramètre et selon un intervalle de temps donné.
   * Open Sky REST API
   * https://openskynetwork.github.io/opensky-api/rest.html#departures-by-airport
   */
    getVolsDepartAeroport(aeroport: IAeroport, debut: Date, fin: Date): Observable<Vol[]> {
      return this.getVolsDepart(aeroport.icao, debut.getTime() / 1000, fin.getTime() / 1000);
    }

    /**
   * Récupération de la liste des vols en arrivée d'un aéroport donné en paramètre et selon un intervalle de temps donné.
   * Open Sky REST API
   * https://openskynetwork.github.io/opensky-api/rest.html#arrivals-by-airport
   */
  getVolsArrivee(code: string, debut: number, fin: number): Observable<Vol[]> {
    return this.http.get<any>(`https://opensky-network.org/api/flights/arrival?airport=${code}&begin=${debut.toFixed()}&end=${fin.toFixed()}`).pipe(
      map((response) => response
        .map((dto: IVolDto) => new Vol(dto))
    ));
  }

    /**
   * Récupération de la liste des vols en arrivée d'un aéroport donné en paramètre et selon un intervalle de temps donné.
   * Open Sky REST API
   * https://openskynetwork.github.io/opensky-api/rest.html#arrivals-by-airport
   */
    getVolsArriveeAeroport(aeroport: IAeroport, debut: Date, fin: Date): Observable<Vol[]> {
      return this.getVolsArrivee(aeroport.icao, debut.getTime() / 1000, fin.getTime() / 1000);
    }
}
