import { Routes } from '@angular/router';
import { ViewAirFranceComponent } from './components/view-airfrance/view-airfrance.component';

export const routes: Routes = [
  {
    path: ':mode', component: ViewAirFranceComponent
  },
  {
    path: '**', redirectTo: 'decollages'
  }
];
